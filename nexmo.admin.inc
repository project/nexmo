<?php

/**
 * @file
 * Provides NEXMO configuration form.
 */

/**
 * NEXMO Configuration form.
 */
function nexmo_admin_form($form) {
  $form['nexmo_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('nexmo_api_key', ''),
    '#description' => t('Your Nexmo account api key'),
    '#required' => TRUE,
  );
  $form['nexmo_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret'),
    '#default_value' => variable_get('nexmo_api_secret', ''),
    '#description' => t('Your Nexmo account api Secret'),
    '#required' => TRUE,
  );
  $form['nexmo_api_sender'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Sender (from)'),
    '#default_value' => variable_get('nexmo_api_sender', ''),
    '#description' => t('Your Default Sender (from)'),
  );
  return system_settings_form($form);
}
