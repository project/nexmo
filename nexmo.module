<?php

/**
 * @file
 * Provides NEXMO implementation methods.
 */

/**
 * Implements hook_gateway_info().
 */
function nexmo_gateway_info() {
  return array(
    'nexmo' => array(
      'name' => 'Nexmo',
      'send' => 'nexmo_send',
      'receive' => TRUE,
      'configure form' => 'nexmo_admin_form',
      'send form' => 'nexmo_send_form',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function nexmo_menu() {
  $items = array();
  $items['admin/smsframework/gateways/nexmo'] = array(
    'title' => 'Nexmo Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nexmo_admin_form'),
    'access arguments' => array('administer nexmosmsgateway'),
    'file' => 'nexmo.admin.inc',
  );
  $items['sms/nexmo/inbound'] = array(
    'title' => 'Nexmo SMS message receiver',
    'page callback' => 'nexmo_receive_message',
    'access arguments' => array('administer nexmosmsgateway'),
  );
  $items['sms/nexmo/receipt'] = array(
    'title' => 'Nexmo SMS receipt receiver',
    'page callback' => 'nexmo_receive_receipt',
    'access arguments' => array('administer nexmosmsgateway'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function nexmo_permission() {
  return array(
    'administer nexmosmsgateway' => array(
      'title' => t('Administer Nexmo SMS Gateway'),
      'description' => t('Nexmo SMS Gateway Inbound and Receipt access'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function nexmo_form_sms_admin_default_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['Nexmo']['id']['#markup']) && $form['Nexmo']['id']['#markup'] == 'nexmo') {
    $form['Nexmo']['configure']['#markup'] = l(t('configure'), 'admin/smsframework/gateways/nexmo');
  }
}

/**
 * Implements hook_help().
 */
function nexmo_help($path, $arg) {
  switch ($path) {
    case 'admin/help#nexmo':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Nexmo SMS Gateway module provides an integration between <a href="https://www.nexmo.com" target="_blank">Nexmo SMS</a> service and <a href="https://www.drupal.org/project/smsframework" target="_blank">SMS framework</a> module.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('This module requires NEXMO subscription. <a href="https://dashboard.nexmo.com/sign-up" target="_blank">Click here</a> to sign-up with NEXMO.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_send().
 */
function nexmo_send($number, $message, $options = array()) {
  $config = array();
  $config['nexmo_api_key'] = variable_get('nexmo_api_key', '');
  $config['nexmo_api_secret'] = variable_get('nexmo_api_secret', '');
  $config['nexmo_api_sender'] = variable_get('nexmo_api_sender', '');
  if ($unicode_message = nexmo_unicode($message)) {
    $message = $unicode_message;
  }
  else {
    $message = rawurlencode($message);
  }
  $number = isset($options['country']) ? $options['country'] . $number : $number;
  $api_key = !empty($config['nexmo_api_key']) ? $config['nexmo_api_key'] : '';
  $api_secret = !empty($config['nexmo_api_secret']) ? $config['nexmo_api_secret'] : '';
  $from = !empty($config['nexmo_api_sender']) ? $config['nexmo_api_sender'] : 'NEXMO';
  $query = 'api_key=' . $api_key . '&api_secret=' . $api_secret . '&from=' . $from . '&to=' . $number . '&text=' . $message . '&status-report-req=1';
  $api_send_url = 'https://rest.nexmo.com/sms/json';
  $http_result = drupal_http_request($api_send_url . '?' . $query);
  if (isset($http_result->error)) {
    return array(
      'status' => FALSE,
      'message' => 'An error occurred during the HTTP request: @error', array(
        '@error' => $http_result->error,
      ),
    );
  }
  if (isset($http_result->data)) {
    if (strpos($http_result->data, 'ERR') !== FALSE) {
      $result = array(
        'status' => FALSE,
        'message' => $http_result->data,
      );
    }
    else {
      $result = array(
        'status' => TRUE,
        'data' => $http_result->data,
      );
    }
  }
  return $result;
}

/**
 * Receive an SMS message and pass it into the SMS Framework.
 */
function nexmo_receive_message() {
  if (!isset($_REQUEST['text'], $_REQUEST['msisdn'], $_REQUEST['to'])) {
    return FALSE;
  }
  else {
    $number = $_REQUEST['msisdn'];
    $message = $_REQUEST['text'];
    $options = array();
    if (array_key_exists('to', $_REQUEST) && !empty($_REQUEST['to'])) {
      $options['receiver'] = $_REQUEST['to'];
    }
    if (array_key_exists('network-code', $_REQUEST) && !empty($_REQUEST['network-code'])) {
      $options['network-code'] = $_REQUEST['network-code'];
    }
    if (array_key_exists('message-timestamp', $_REQUEST) && !empty($_REQUEST['message-timestamp'])) {
      $options['message-timestamp'] = $_REQUEST['message-timestamp'];
    }
    if (array_key_exists('messageId', $_REQUEST) && !empty($_REQUEST['messageId'])) {
      $options['messageId'] = $_REQUEST['messageId'];
    }
    sms_incoming($number, $message, $options);
    print "response 200 OK";
  }
}

/**
 * Receive a message send receipt from Nexmo.
 */
function nexmo_receive_receipt() {
  $number = $_REQUEST['msisdn'];
  $gateway_status = $_REQUEST['status'];
  $reference = (array_key_exists('messageId', $_REQUEST)) ? $_REQUEST['messageId'] : NULL;
  $options = array();
  $options['reference'] = $reference;
  if (array_key_exists('to', $_REQUEST) && !empty($_REQUEST['to'])) {
    $options['receiver'] = $_REQUEST['to'];
  }
  if (array_key_exists('network-code', $_REQUEST) && !empty($_REQUEST['network-code'])) {
    $options['network-code'] = $_REQUEST['network-code'];
  }
  if (array_key_exists('price', $_REQUEST) && !empty($_REQUEST['price'])) {
    $options['price'] = $_REQUEST['price'];
  }
  if (array_key_exists('messageId', $_REQUEST) && !empty($_REQUEST['messageId'])) {
    $options['messageId'] = $_REQUEST['messageId'];
  }
  sms_receipt($number, $reference, $gateway_status, $options);
  print "response 200 OK";
}

/**
 * Returns custom additions to be added to the send forms.
 */
function nexmo_send_form() {
  $options = array(
    "" => "- Select Country -",
  ) + sms_country_codes();
  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#multiple' => FALSE,
    '#options' => $options,
    '#default_value' => -1,
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Converts a string to UCS-2 encoding if necessary.
 */
function nexmo_unicode($message) {
  if (function_exists('iconv')) {
    $latin = @iconv('UTF-8', 'ISO-8859-1', $message);
    if (strcmp($latin, $message)) {
      $arr = unpack('H*hex', @iconv('UTF-8', 'UCS-2BE', $message));
      return strtoupper($arr['hex']) . '&unicode=1';
    }
  }
  return FALSE;
}
