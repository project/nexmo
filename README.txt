
Description
===========

This module provides integration between NEXMO SMS service and
Drupal SMS framework project.

Dependencies
============
1.Drupal SMS Framework module.
2.Requires NEXMO subscription.( https://www.nexmo.com )

Installation
============
1.Download and install the module as usual.
2.Go to "admin/smsframework/gateways/nexmo_sms_gateway" and
configure your API key, secret code of your NEXMO subscription.
